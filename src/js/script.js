const palindromeMessage = document.querySelector(".palindrome-message");
const result = document.querySelector(".checked-palindrome");
const input = document.querySelector("#palindrome");
const button = document.querySelector("button");

// Add an event when user presses "Enter" key
document.addEventListener("keydown", (e) => {
	if (e.code === "Enter" || e.code === "NumpadEnter") {
		handleUserInput();
	}
});

// Add an event when user click's the button
button.addEventListener("click", handleUserInput);


function handleUserInput() {
	let text = input.value;
	// Store the checked text
	const checked = checkPalindrome(text);
	// Display the message in the UI
	displayMessage(checked, text);
	
	// Reset the value of the input
	document.querySelector("#palindrome").value = "";
}

function checkPalindrome(str) {
	// Remove all non-alphanumeric characters
	let strippedString = str.replace(/[^A-Z0-9]/ig, "").toLowerCase();
	// Reverse the initial string
	let reversedString = strippedString.split("").reverse().join("").toLowerCase();
	
	return strippedString === reversedString;
}


function displayMessage(isPalindrome, userInput) {
	let strippedText = userInput.replace(/[^A-Z0-9]/ig, "");
	
	result.innerHTML = `"${userInput}"`;
	if (isPalindrome) {
		palindromeMessage.innerHTML = `- is a palindrome`;
	} else {
		palindromeMessage.innerHTML = `- is <strong>NOT</strong> a palindrome`;
	}
	
	if (strippedText === "") {
		palindromeMessage.innerHTML = "";
		result.innerHTML = ` Please add text`;
	}
}