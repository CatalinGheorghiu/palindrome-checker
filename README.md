<h1>Palindrome Checker</h1>


<p>
Just a simple Palindrome Checker app, responsive and cross-browser compatible, made in vanilla JS and Scss.</p>

<a href="https://ugly-palindrome-checker.netlify.app/">View Live Project</a>

<img src="https://res.cloudinary.com/catalin/image/upload/v1622527549/dev_setup/obkpcmkmlqyov3roqjyd.png" alt="Demo"/>
