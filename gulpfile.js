const {src, dest, watch, series} = require("gulp");
const sass = require("gulp-sass");
const prefix = require("gulp-autoprefixer");
const minify = require("gulp-clean-css");
const terser = require("gulp-terser");
const browserSync = require("browser-sync").create();

// Html
function html() {
	return src('./src/*.html')
	.pipe(dest('./public'))
	.pipe(browserSync.stream());
}

// Compile scss into css
function compilescss() {
	// where is my scss file
	return src("./src/scss/**/*.scss")
	// pass that file trough the sass compiler
	.pipe(sass().on("error", sass.logError))
	// prefix
	.pipe(prefix("last 2 versions"))
	// minify
	.pipe(minify())
	// where do I save the compiled CSS
	.pipe(dest("./public/css"))
	// Stream changes to all browsers
	.pipe(browserSync.stream());
}

// Javascript
function jsmin() {
	return src("./src/js/*.js")
	.pipe(terser())
	.pipe(dest("./public/js"))
	.pipe(browserSync.stream());
}

// Watch task
function watchTask() {
	browserSync.init({
		server: {
			baseDir: "./public"
		}
	});
	watch("./src/scss/**/*.scss", compilescss);
	watch("./src/js/**/*.js",jsmin).on("change", browserSync.reload);
	watch("./src/*.html",html).on("change", browserSync.reload);
}

// Default gulp
exports.default = series(
	html,
	compilescss,
	jsmin,
	watchTask
);
exports.watchTask = watchTask;

